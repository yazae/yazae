

Différentes sources qui donnent des pistes de réflexion

-   CodePen - Féminiser les textes (liste) - [https://codepen.io/vincent-valentin/full/woGLVL/](https://codepen.io/vincent-valentin/full/woGLVL/)

  

-   À celleux qui me lisent - romy.tetue.net  - [http://romy.tetue.net/a-celleux-qui-me-lisent](http://romy.tetue.net/a-celleux-qui-me-lisent)

  

-   Inclusivité & accessibilité - Lunatopia - [http://lunatopia.fr/blog/inclusivite-accessibilite](http://lunatopia.fr/blog/inclusivite-accessibilite) (ça donne une réponse sur comment est lu le texte avec les point médians : avec les lettres prononcées distinctement apparemment ( à test )

  

En vrac (pas toutes les sources se valent, mais c'est intéressant) :

-   [https://cafaitgenre.org/2013/12/10/feminisation-de-la-langue-quelques-reflexions-theoriques-et-pratiques/comment-page-1/](https://cafaitgenre.org/2013/12/10/feminisation-de-la-langue-quelques-reflexions-theoriques-et-pratiques/comment-page-1/)

-   [http://www.madmoizelle.com/guide-langage-non-sexiste-109220](http://www.madmoizelle.com/guide-langage-non-sexiste-109220)

-   [http://romy.tetue.net/feminiser-au-point-median](http://romy.tetue.net/feminiser-au-point-median)

-   [http://feministesentousgenres.blogs.nouvelobs.com/tag/sexisme+et+acad%C3%A9mie+fran%C3%A7aise](http://feministesentousgenres.blogs.nouvelobs.com/tag/sexisme+et+acad%C3%A9mie+fran%C3%A7aise)

-   [https://fr.wikipedia.org/wiki/Langage_%C3%A9pic%C3%A8ne](https://fr.wikipedia.org/wiki/Langage_%C3%A9pic%C3%A8ne)

-   [https://biscuitsdefortune.wordpress.com/2015/06/20/loqlf-et-la-feminisation-survol-et-critiques/](https://biscuitsdefortune.wordpress.com/2015/06/20/loqlf-et-la-feminisation-survol-et-critiques/)

-   [http://www.theholyculotte.com/2016/11/sexisme-de-la-langue-francaise-et-ecriture-inclusive-ressources/](http://www.theholyculotte.com/2016/11/sexisme-de-la-langue-francaise-et-ecriture-inclusive-ressources/)

  

À propos du neutre et de l'inclusif, UESG : 

-    [http://uniqueensongenre.eklablog.fr/petit-dico-de-francais-neutre-inclusif-a120741542](http://uniqueensongenre.eklablog.fr/petit-dico-de-francais-neutre-inclusif-a120741542)

  

+ un scan de l'article de Well Well Well # 2 sur l'écriture inclusive : [https://framadrop.org/r/UASg47w75e#pH1RzW/rU3WB4j3xnGwXdFBLVl62coswgSWnIu4nOyY=](https://framadrop.org/r/UASg47w75e#pH1RzW/rU3WB4j3xnGwXdFBLVl62coswgSWnIu4nOyY=) (le lien expire le 10/08/17)

  

(ça viens de mes marques-pages Framanotes sur le sujet, si vous utilisez framanotes je peux partager le tableau avec vous)